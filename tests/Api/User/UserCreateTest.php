<?php

declare(strict_types=1);

namespace App\Tests\Api\User;

use App\Domain\Entity\User;
use App\Tests\Api\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserCreateTest extends ApiTestCase
{
    public function testSuccess(): void
    {
        $request = [
            'email' => 'test1@example.com',
            'password' => '12345',
            'username' => 'test123'
        ];

        $response = static::sendPost('/api/users', $request);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_CREATED);

        $user = self::$em->getRepository(User::class)->findBy(['email' => 'test1@example.com']);
        $this->assertNotNull($user);
    }
}