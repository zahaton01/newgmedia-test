<?php

declare(strict_types=1);

namespace App\HTTP\Controller;

use App\Domain\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/users/{id}', name: 'api_users_view', methods: ['GET'])]
class ViewUserAction extends AbstractController
{
    public function __invoke(User $user)
    {
        return $this->json(
            $user,
            200,
            [],
            [
                'groups' => [
                    User::API_SERIALIZER_GROUP,
                ]
            ]
        );
    }
}