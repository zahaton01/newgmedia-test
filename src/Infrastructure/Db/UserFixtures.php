<?php

declare(strict_types=1);

namespace App\Infrastructure\Db;

use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    public const TEST_USER_EMAIL = 'test@example.com';
    public const TEST_USER_USERNAME = 'test';

    public function __construct(
        private readonly UserPasswordHasherInterface $passwordHasher
    ) {}

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User(uniqid(), uniqid());
            $user->setPassword($this->passwordHasher->hashPassword($user, '123'));

            $manager->persist($user);
        }

        $user = new User(self::TEST_USER_EMAIL, self::TEST_USER_USERNAME);
        $user->setPassword($this->passwordHasher->hashPassword($user, '123'));
        $manager->persist($user);

        $manager->flush();
    }
}