<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Command\CreateUserCommand;
use App\Domain\Entity\User;
use App\Infrastructure\Exception\ModelValidationException;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateUserCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly ValidatorInterface $validator
    ) {}

    public function __invoke(CreateUserCommand $command): void
    {
        $user = new User($command->email, $command->username);

        $hashedPassword = $this->passwordHasher->hashPassword($user, $command->password);
        $user->setPassword($hashedPassword);

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new ModelValidationException($errors);
        }

        $this->em->persist($user);
        $this->em->flush();
    }
}