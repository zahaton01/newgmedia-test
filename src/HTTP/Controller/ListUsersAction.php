<?php

declare(strict_types=1);

namespace App\HTTP\Controller;

use App\Domain\Entity\User;
use App\Domain\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/users', name: 'api_users_list', methods: ['GET'])]
class ListUsersAction extends AbstractController
{
    public function __construct(
        private readonly UserRepository $userRepository
    ) {}

    public function __invoke(Request $request)
    {
        $users = $this->userRepository->getFilteredList(
            $request->query->get('email'),
            $request->query->get('username')
        );

        return $this->json(
            $users,
            200,
            [],
            [
                'groups' => [
                    User::API_SERIALIZER_GROUP,
                ]
            ]
        );
    }
}