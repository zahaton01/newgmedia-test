ENV = --env-file ./.env.local

up:
	docker-compose up -d
down:
	docker-compose down
php:
	docker exec -it app-php bash
mysql:
	docker exec -it app-mysql bash
init-dev:
	docker-compose exec app bash -c "\
		composer i &&\
		bin/console doctrine:database:drop --force --if-exists --env=dev &&\
		bin/console doctrine:database:create --env=dev &&\
		bin/console doctrine:migrations:migrate --env=dev --no-interaction \
	"
test:
	docker-compose exec app bash -c "\
		composer i &&\
		bin/console doctrine:database:drop --force --if-exists --env=test &&\
		bin/console doctrine:database:create --env=test &&\
		bin/console doctrine:migrations:migrate --env=test --no-interaction &&\
		bin/console d:f:l --env=test --no-interaction &&\
		vendor/bin/phpunit \
	"
