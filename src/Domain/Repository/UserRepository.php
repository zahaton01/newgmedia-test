<?php

declare(strict_types=1);

namespace App\Domain\Repository;

use App\Domain\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method null|User find($id, $lockMode = null, $lockVersion = null): ?User
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return User[]
     */
    public function getFilteredList(?string $email = null, ?string $username = null): array
    {
        $qb = $this->createQueryBuilder('u');

        if (null !== $email) {
            $qb->andWhere('lower(u.email) like :email');
            $qb->setParameter('email', '%' . mb_strtolower($email) . '%');
        }

        if (null !== $username) {
            $qb->andWhere('lower(u.username) like :username');
            $qb->setParameter('username', '%' . mb_strtolower($username) . '%');
        }

        return $qb->getQuery()->getResult();
    }
}