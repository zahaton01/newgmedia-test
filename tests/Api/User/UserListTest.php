<?php

declare(strict_types=1);

namespace App\Tests\Api\User;

use App\Infrastructure\Db\UserFixtures;
use App\Tests\Api\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserListTest extends ApiTestCase
{
    private const ENDPOINT = '/api/users';

    public function testSuccess(): void
    {
        $response = static::sendGet(self::ENDPOINT);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_OK);
        $data = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertNotEmpty($data);

        $response = static::sendGet(self::ENDPOINT, ['email' => UserFixtures::TEST_USER_EMAIL]);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_OK);
        $data = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertNotEmpty($data);

        $response = static::sendGet(self::ENDPOINT, ['username' => UserFixtures::TEST_USER_USERNAME]);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_OK);
        $data = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertNotEmpty($data);

        $response = static::sendGet(self::ENDPOINT, ['username' => 'not-exist']);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_OK);
        $data = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->assertEmpty($data);
    }
}