<?php

declare(strict_types=1);

namespace App\HTTP\Controller;

use App\Application\Command\EditUserCommand;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/users/{id}', name: 'api_users_edit', methods: ['PATCH'])]
class EditUserAction extends AbstractController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {}

    public function __invoke(int $id, EditUserCommand $command): JsonResponse
    {
        $command->id = $id;
        $this->commandBus->execute($command);

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}