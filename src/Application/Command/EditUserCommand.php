<?php

declare(strict_types=1);

namespace App\Application\Command;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditUserCommand
{
    #[NotBlank]
    public int $id = 0;

    #[NotBlank]
    #[Email]
    public ?string $email = null;

    #[NotBlank]
    public ?string $password = null;

    #[NotBlank]
    public ?string $username = null;
}