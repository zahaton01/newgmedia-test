<?php

declare(strict_types=1);

namespace App\Domain\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Index;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[Entity]
#[Index(columns: ['email'], name: 'email_index')]
#[UniqueEntity(fields: ['email', 'username'])]
class User implements PasswordAuthenticatedUserInterface
{
    public const API_SERIALIZER_GROUP = 'user';

    #[Id]
    #[GeneratedValue]
    #[Column(type: 'integer')]
    #[Groups([self::API_SERIALIZER_GROUP])]
    private int $id;

    #[Column(type: 'string', unique: true, nullable: false)]
    #[Groups([self::API_SERIALIZER_GROUP])]
    private string $email;

    #[Column(type: 'string', nullable: false)]
    private string $password;

    #[Column(type: 'string', unique: true, nullable: false)]
    #[Groups([self::API_SERIALIZER_GROUP])]
    private string $username;

    #[Column(type: 'datetime', nullable: false)]
    #[Groups([self::API_SERIALIZER_GROUP])]
    private DateTimeInterface $createdAt;

    public function __construct(string $email, string $username)
    {
        $this->email = $email;
        $this->username = $username;
        $this->password = '';
        $this->createdAt = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}