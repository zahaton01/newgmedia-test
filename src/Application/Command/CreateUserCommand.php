<?php

declare(strict_types=1);

namespace App\Application\Command;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateUserCommand
{
    #[NotBlank]
    #[Email]
    public string $email;

    #[NotBlank]
    public string $password;

    #[NotBlank]
    public string $username;
}