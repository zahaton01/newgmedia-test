<?php

declare(strict_types=1);

namespace App\HTTP\Controller;

use App\Application\Command\CreateUserCommand;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/users', name: 'api_users_create', methods: ['POST'])]
class CreateUserAction extends AbstractController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {}

    public function __invoke(CreateUserCommand $command): JsonResponse
    {
        $this->commandBus->execute($command);

        return $this->json(null, Response::HTTP_CREATED);
    }
}