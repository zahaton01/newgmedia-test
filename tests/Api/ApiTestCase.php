<?php

declare(strict_types=1);

namespace App\Tests\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiTestCase extends WebTestCase
{
    protected static ?EntityManagerInterface $em = null;
    protected static ?KernelBrowser $client = null;

    public function setUp(): void
    {
        self::$client = static::createClient();
        self::$em = self::getContainer()->get(EntityManagerInterface::class);
        self::$em->getConnection()->setAutoCommit(false);
        self::$em->getConnection()->beginTransaction();
    }

    public function tearDown(): void
    {
        if (self::$em->getConnection()->isTransactionActive()) {
            self::$em->getConnection()->rollBack();
            self::$em->close();
            self::$em = null;
        }

        parent::tearDown();
    }

    protected static function sendGet(string $uri, array $parameters = []): Response
    {
        self::$client->request(
            'GET',
            $uri . '?' . http_build_query($parameters),
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            null
        );

        return self::$client->getResponse();
    }

    protected static function sendPost(string $uri, array $data = []): Response
    {
        $headers = ['CONTENT_TYPE' => 'application/json'];

        self::$client->request(
            'POST',
            $uri,
            [],
            [],
            $headers,
            json_encode($data)
        );

        return self::$client->getResponse();
    }

    protected static function sendPatch(string $uri, array $data = []): Response
    {
        $headers = ['CONTENT_TYPE' => 'application/json'];

        self::$client->request(
            'PATCH',
            $uri,
            [],
            [],
            $headers,
            json_encode($data)
        );

        return self::$client->getResponse();
    }
}