<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Command\EditUserCommand;
use App\Domain\Repository\UserRepository;
use App\Infrastructure\Exception\ModelValidationException;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EditUserCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserPasswordHasherInterface $passwordHasher,
        private readonly UserRepository $userRepository,
        private readonly ValidatorInterface $validator
    ) {}

    public function __invoke(EditUserCommand $command): void
    {
        $user = $this->userRepository->find($command->id);

        if (null === $user) {
            throw new NotFoundHttpException();
        }

        if (null !== $command->email) {
            $user->setEmail($command->email);
        }

        if (null !== $command->username) {
            $user->setUsername($command->username);
        }

        if (null !== $command->password) {
            $hashedPassword = $this->passwordHasher->hashPassword($user, $command->password);
            $user->setPassword($hashedPassword);
        }

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            throw new ModelValidationException($errors);
        }

        $this->em->flush();
    }
}