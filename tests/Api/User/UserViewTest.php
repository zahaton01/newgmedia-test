<?php

declare(strict_types=1);

namespace App\Tests\Api\User;

use App\Domain\Entity\User;
use App\Tests\Api\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;

class UserViewTest extends ApiTestCase
{
    public function testSuccess(): void
    {
        /** @var User $user */
        $user = self::$em->getRepository(User::class)->findOneBy([]);
        $response = static::sendGet(sprintf('/api/users/%s', $user->getId()));

        $this->assertTrue($response->getStatusCode() === Response::HTTP_OK);
    }
}