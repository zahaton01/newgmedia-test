<?php

declare(strict_types=1);

namespace App\Infrastructure\Exception;

use Exception;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ModelValidationException extends Exception
{
    /** @var ConstraintViolationInterface[] */
    private array $errors;

    public function __construct(ConstraintViolationListInterface $errors)
    {
        parent::__construct('Validation failed.', 400);

        foreach ($errors as $error) {
            $this->errors[] = $error;
        }
    }

    /**
     * @return ConstraintViolationInterface[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}