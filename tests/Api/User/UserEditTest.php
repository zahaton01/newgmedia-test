<?php

declare(strict_types=1);

namespace App\Tests\Api\User;

use App\Domain\Entity\User;
use App\Infrastructure\Db\UserFixtures;
use App\Tests\Api\ApiTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserEditTest extends ApiTestCase
{
    public function testSuccess(): void
    {
        $request = [
            'email' => 'test2@example.com',
            'password' => '123456',
            'username' => 'test1237'
        ];

        $user = self::$em->getRepository(User::class)->findOneBy(['email' => UserFixtures::TEST_USER_EMAIL]);
        $response = static::sendPatch(sprintf('/api/users/%s', $user->getId()), $request);

        $this->assertTrue($response->getStatusCode() === Response::HTTP_NO_CONTENT);

        self::$em->refresh($user);

        $this->assertSame($user->getEmail(), 'test2@example.com');
        $this->assertSame($user->getUsername(), 'test1237');

        $passwordHasher = static::getContainer()->get(UserPasswordHasherInterface::class);
        $this->assertTrue($passwordHasher->isPasswordValid($user, '123456'));
    }
}