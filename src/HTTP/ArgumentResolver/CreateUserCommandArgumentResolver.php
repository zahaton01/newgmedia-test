<?php

declare(strict_types=1);

namespace App\HTTP\ArgumentResolver;

use App\Application\Command\CreateUserCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class CreateUserCommandArgumentResolver extends AbstractArgumentResolver
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return parent::supports($request, $argument) && CreateUserCommand::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        yield $this->deserialize($request, CreateUserCommand::class);
    }
}