<?php

declare(strict_types=1);

namespace App\HTTP\ArgumentResolver;

use App\Application\Command\EditUserCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class EditUserCommandArgumentResolver extends AbstractArgumentResolver
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return parent::supports($request, $argument) && EditUserCommand::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): \Generator
    {
        yield $this->deserialize($request, EditUserCommand::class);
    }
}