<?php
declare(strict_types=1);

namespace App\Infrastructure\Messenger\CommandBus;

use Symfony\Component\Messenger\MessageBusInterface;

class CommandBus implements CommandBusInterface
{
    public function __construct(
        private readonly MessageBusInterface $messageBus
    ) {}

    public function execute($command)
    {
        $this->messageBus->dispatch($command);
    }
}
