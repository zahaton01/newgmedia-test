<?php

declare(strict_types=1);

namespace App\Infrastructure\Messenger\Middleware;

use App\Infrastructure\Exception\ModelValidationException;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MessageValidationMiddleware implements MiddlewareInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {}

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $this->validate($envelope->getMessage());

        return $stack->next()->handle($envelope, $stack);
    }

    private function validate(object $message): void
    {
        $errors = $this->validator->validate($message);

        if (count($errors) > 0) {
            throw new ModelValidationException($errors);
        }
    }
}