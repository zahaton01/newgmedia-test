<?php

declare(strict_types=1);

namespace App\HTTP\ArgumentResolver;

use JsonException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;

abstract class AbstractArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        protected SerializerInterface $serializer
    ) {}

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return str_starts_with($request->get('_route'), 'api_');
    }

    final protected function deserialize(Request $request, string $target)
    {
        return $this->serializer->deserialize($request->getContent(), $target, 'json');
    }
}